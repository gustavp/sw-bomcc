#!/usr/bin/env python3
import argparse
import csv

from common import smart_open_w, smart_open_r

if __name__ == '__main__':
    """The BOM Compiler"""
    parser = argparse.ArgumentParser(description='Compile some BOMs into a super BOM')
    parser.add_argument('boms', metavar='BOM', nargs='+',
                        help='BOMS to compile/merge')
    parser.add_argument('--output', '-o', metavar='OUTFILE', default=None,
                        help='Output file')
    parser.add_argument('--encoding', '-e', default='ISO-8859-1',
                        help='Text encoding, uses ISO-8859-1 by default because AD is shit')
    parser.add_argument('--print-keys', action='store_true',
                        help='Print resolved keys')
    args = parser.parse_args()

    keys = set()
    items = []
    for bom in args.boms:
        with smart_open_r(bom, newline='', encoding=args.encoding) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                keys = keys | set(row.keys())
                items.append(row)

    if args.print_keys:
        print(keys)

    with smart_open_w(args.output, 'w', newline='') as outfile:
        writer = csv.DictWriter(outfile, fieldnames=keys)
        writer.writeheader()
        writer.writerows(items)
