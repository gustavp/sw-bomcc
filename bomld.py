#!/usr/bin/env python3
import argparse
import collections
import csv
import json
import re
import urllib.parse
import urllib.request


from common import smart_open_r, smart_open_w


class BomItem(collections.abc.MutableMapping):

    def __init__(self, columns, *args, **kwargs) -> None:
        super().__init__()
        # Store arbitrary parameters as a dict
        self.store = columns
        self.update(dict(*args, **kwargs))  # use the free update to set keys

    def copy(self, prefix=None):
        item = BomItem(self.store.copy())
        if prefix:
            item.designators = ['{}.{}'.format(prefix, x) for x in self.designators]
        return item

    def explode(self):
        items = []
        for designator in self.designators:

            m = re.match(r"(?P<prefix>[a-zA-Z]+)(?P<start>\d+)-(?P<stop>\d+)", designator)
            if m:
                prefix = m.group('prefix')
                start = m.group('start')
                stop = m.group('stop')

                for x in range(int(start), int(stop) + 1):
                    item = BomItem(self.store.copy())
                    item.designators = prefix + str(x)
                    item.quantity = 1
                    items.append(item)
            else:
                item = BomItem(self.store.copy())
                item.designators = designator
                item.quantity = 1
                items.append(item)

        return items

    def merge(self, item):

        # Append new designators
        designators = self.designators
        designators.extend(item.designators)
        self.designators = designators

        self.quantity = self.quantity + item.quantity

    @property
    def designators(self):
        return [x.strip() for x in str.split(self.__getitem__('Designator'), ',')]

    @designators.setter
    def designators(self, d):
        if isinstance(d, list):
            self.__setitem__('Designator', ','.join(d))
        elif isinstance(d, str):
            self.__setitem__('Designator', d)

    @property
    def quantity(self):
        return int(self.__getitem__('Quantity'))

    @quantity.setter
    def quantity(self, d):
        self.__setitem__('Quantity', str(d))

    @property
    def name(self):
        return self.__getitem__('Name')

    @name.setter
    def name(self, d):
        self.__setitem__('Name', d)

    # Act as a dict
    def __getitem__(self, key):
        return self.store[key]

    def __setitem__(self, key, value):
        self.store[key] = value

    def __delitem__(self, key):
        del self.store[key]

    def __iter__(self):
        return iter(self.store)

    def __len__(self):
        return len(self.store)

    def __str__(self) -> str:
        return '\'%s\' - %s : %d' % (self.name, self.designators, self.quantity)


def read_bom(file):
    items = []
    keys = set()
    reader = csv.DictReader(file)
    for row in reader:
        if not keys:
            keys = set(row.keys())

        item = BomItem(row)
        items.append(item)

    return items, keys


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Link a BOM with dependencies')
    parser.add_argument('bom', metavar='BOM',
                        help='BOM file to link')
    parser.add_argument('--output', '-o', metavar='OUTFILE',
                        help='Output file')
    parser.add_argument('--encoding', '-e', default='ISO-8859-1',
                        help='Text encoding, uses ISO-8859-1 by default because AD is shit')
    parser.add_argument('--unique', '-u', default=None, metavar='KEY',
                        help='Sort BOM and remove duplicates of specified key')
    parser.add_argument('--explode', '-E', action='store_true',
                        help='Explode BOM into items of only one')

    args = parser.parse_args()

    keys = None
    items = None

    # Open and read BOM
    try:
        with smart_open_r(args.bom, newline='', encoding=args.encoding) as csvfile:
            print("Linking %s" % args.bom)
            items, keys = read_bom(csvfile)
    except IOError as e:
        print('Cannot read %s' % args.bom)
        exit(1)

    # Link subassemblies
    remove_subassemby = []
    append_items = []
    for item in items:
        m = re.match(r"\$sub:(?P<file>.*)", item.name)

        # Item is a subassembly
        if m:
            file = m.group('file')

            # OPen and add subassembly
            with open(file, newline='', encoding='utf8') as subfile:
                subbom, subkeys = read_bom(subfile)

                # Add any missing keys from sub-BOM
                keys = keys | subkeys

                # Prefix and add subassembly items to BOM
                for designator in item.designators:
                    print(' / %s ~ %s' % (designator, file))
                    if not len(subbom):
                        print('   (empty)')
                    else:
                        for subitem in subbom:
                            prefix_sub = subitem.copy(designator)
                            print('   * %s' % prefix_sub)
                            append_items.append(prefix_sub)

                # Remove original subassembly item
                remove_subassemby.append(item)

        # Item is a normal item, don't care.
        else:
            print(' * %s' % item)

    # Add/remove subassembly items
    for subassembly in remove_subassemby:
        items.remove(subassembly)
    for app in append_items:
        items.append(app)

    # Flatten BOM and merge duplicates
    if args.unique:
        print('Flattening with parameter \'{}\'...'.format(args.unique))

        uniques = {}
        leftovers = []

        for item in items:
            parts = item.explode()

            try:
                if item[args.unique] in uniques:
                    if not item[args.unique] or item[args.unique].strip() == '':
                        raise KeyError

                    # print('{}={}'.format(item[args.unique], uniques[item[args.unique]][args.unique]))
                    uniques[item[args.unique]].merge(item)
                    print(' Merged {} into {}...'.format(item, item[args.unique]))
                else:
                    uniques[item[args.unique]] = item
            except KeyError:
                print(" Warning: {} does not contain key or has empty key, will not be merged!".format(item))
                leftovers.append(item)

        items = list(uniques.values())
        items.extend(leftovers)

    if args.explode:
        print('Exploding...')
        new_items = []
        for item in items:
            parts = item.explode()
            new_items.extend(parts)
        items = new_items

    # Write linked BOM
    print('Writing BOM to \'%s\'...' % args.output)
    with smart_open_w(args.output, 'w', newline='', encoding='utf8') as outfile:
        writer = csv.DictWriter(outfile, fieldnames=keys)
        writer.writeheader()
        writer.writerows(items)
    print('Done!')
